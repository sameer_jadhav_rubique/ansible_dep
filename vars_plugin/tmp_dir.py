from ansible import constants as C
import os

class VarsModule(object):

    def __init__(self, inventory):
        pass

    def run(self, host, vault_password=None):
        cwd = os.getcwd()
        config_dir = cwd+'/config'
        home_dir = os.path.expanduser('~')
        store_dir = home_dir + '/store/'
        configDict = dict()
        configDict['_config_dir_'] = config_dir
        configDict['_store_dir_'] = store_dir
        return configDict
