# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Automating things for rubique
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests  
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Lint and get a Hint.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact 
* Just Go with the flow.
* Not with HR.

### Deployment instructions ###

* [For Installing Ansible](http://docs.ansible.com/ansible/intro_installation.html)
* SSH Connection config on each Node
  <```
    ssh-keygen
    ssh-copy-id user@ipaddress
  ```
